export const ActionA = 'Action_A'
export const ActionB = 'Action_B'

export const Action_A = payload => ({
    type: ActionA,
    payload
})
export const Action_B = payload => ({
    type: ActionB,
    payload,
    recieved_data: json.data.children.map(child => child.data),
    recievedAt: Date.now()
})
const url = google.com
const fetchData = payload => dispatch => {
    dispatch(Action_A(payload))
    return fetch (`${url}+${payload}`)
    .then(response => response.json())
    .then(json => dispatch(Action_B(payload,json)))
}
const shouldFetchData = (state,payload) => {
    const data = state.mappedData[index]
    if (!data){
        return true
    }
    if (data.isFetching) {
        return false
    }
    return data.didInvalidate
}

export const fetchDataIfNeeded = payload => (dispatch,getState) => {
    if (shouldFetchData(getState(),payload)) {
        return dispatch(fetchData(payload))
    }
}