import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {ActionA, ActionB} from '../actions'
import ComponentA from '../components/ComponentA'
import ComponentB from '../components/ComponentB'

class App extends Component {
    static propTypes = {
        propA: PropTypes.string.isRequired
    }
    componentDidMount() {
        const {dispatch, propA} = this.props
        dispatch(ActionA(propA))
    }
    render(){
        const {propA} = this.props
    }
}
const mapStateToProps = state => {
    const {propA} = state
    return {
        propA
    }
}
    
export default connect(mapStateToProps)(App)


