import { combineReducers } from 'redux'
import { ActionA, ActionB} from '../actions'

const data = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
}, action) => {
    switch(action.type){
        case ActionA:
            return {
                ...state,
                isFetching: true,
                didInvalidate: false
            }
        case ActionB:
            return {
                ...state,
                isFetching: false,
                items: actions.data,
                lastUpdated: action.receivedAt
            }

        default:
            return state
    }
}

const reducerA = ( state = { }, action)=>{
    switch (action.type) {
        case ActionA:
        case ActionB:
            return {
                ...state,
                [action.payload]: data(state[action.payload], action)
            }
        default:
            return state
    }
}

const rootReducer = combineReducers({
    reducerA
})

export default rootReducer